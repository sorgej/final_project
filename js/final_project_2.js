// Final Project 
// Project is to take user input to set a view in MapQuest API

window.onload = function() {

// 1 validate input fields
// input user string
// output modified html / css + classes. returns true or false
const validateAddress = function (inputEl, submitEvent) {
    // const target = submitEvent.target;
    //var constraint = new RegExp(constraints, "");

    const errorEl = inputEl.parentElement.querySelector('.error');
    
    if (inputEl.value === '' || inputEl.value.length < 3) {
        console.log('Bad input');

        const labelEL  = inputEl.parentElement.querySelector('label');

        errorEl.innerHTML = `${labelEL.innerText} must be at least 3 characters long.`;
        inputEl.parentElement.classList.add('invalid');
        return false;
    } else {
        inputEl.parentElement.classList.remove('invalid');
        errorEl.innerHTML = ``;
        return true;
    };
    // return false or true
}
// todo extra ... remove invalid immediately after typing. see matt'a code

// 2 Save cookie from input 
const storeLocation = function() {
    const lastAddress = document.getElementsByName('address')[0].value;
    //console.log(lastAddress)
    if (lastAddress !== '') {
            localStorage.setItem('last-address',lastAddress);
    }
}

// 3 Display subtitle after 1 second (1000 milliseconds)
let subtitle = document.getElementsByTagName('h4')[0];
setTimeout(function() {
    subtitle.style.display = 'inline-block';
  },
  1000
);

// 4 put it all together
observeTraffic = function(inputEl, submitEvent) {
    // see if address is correct
    submitEvent.preventDefault();
    const hasAddress = validateAddress(inputEl, submitEvent);
    if (hasAddress){
        storeLocation();
        addressString =inputEl.value;
        L.mapquest.geocoding().geocode(addressString); // INPUT USER ADDRESS STRING
        //console.log(addressString);
    };
}


// create map
const inputEl = document.getElementsByClassName('validate-input')[0];

// !!API_KEY ? L.mapquest.key = API_KEY : 
L.mapquest.key = process.env.API_KEY;
var map  = L.mapquest.map('map', {
    center: [0, 0], //[37.7749, -122.4194]
    layers: L.mapquest.tileLayer('map'),
    zoom: 10
});

let addressString =inputEl.value;
let lastAddress = localStorage.getItem('last-address');

if ( addressString === '' &&  lastAddress === null) {
    addressString = 'universty+washington,wa'; // set default
} else if ( addressString === '' &&  lastAddress !== '') {
    addressString = lastAddress; // set last address
};
L.mapquest.geocoding().geocode(addressString); // INPUT USER ADDRESS STRING

map.addControl(L.mapquest.control());

map.addLayer(L.mapquest.trafficLayer());
map.addLayer(L.mapquest.incidentsLayer());
map.addLayer(L.mapquest.marketsLayer());


// 6 Event Listeners. updates based on input
const formEl = document.getElementsByTagName('form')[0]
    .addEventListener('submit', function(e) {
        observeTraffic(inputEl, e); 
        e.preventDefault();
    });
}; // ends on window.onload
