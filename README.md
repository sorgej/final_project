## Final Project

An API key is required to use this app. It has been submitted in the separate Final Project link.

The file should be named 'api-key.js', and it should be stored in the /js subfolder.